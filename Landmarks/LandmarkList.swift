/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A view showing a list of landmarks.
*/

import SwiftUI

struct LandmarkList: View {
    @State var showFavoritesOnly = false

    var body: some View {
        NavigationView {
            VStack{
                Toggle(isOn: $showFavoritesOnly) {
                      Text("Favorites only")
                }.padding(.horizontal,20)
                ScrollView (.horizontal, showsIndicators: false){
                    HStack{
                        ForEach(landmarkData) { landmark in
                            if !self.showFavoritesOnly || landmark.isFavorite {
                                NavigationLink(destination: LandmarkDetail(landmark: landmark)) {
                                    LandmarkRow(landmark: landmark).padding(.all,15)
                                }
                            }
                        }
                    }
                    }.frame(height:60)
                    
                List {
                    /*Toggle(isOn: $showFavoritesOnly) {
                        Text("Favorites only")
                    }*/
                    ForEach(landmarkData) { landmark in
                        if !self.showFavoritesOnly || landmark.isFavorite {
                            NavigationLink(destination: LandmarkDetail(landmark: landmark)) {
                                LandmarkRow(landmark: landmark)
                            }
                        }
                    }
                }
            }
           
             .navigationBarTitle(Text("Landmarks"),displayMode: .automatic)
            
        }
    }
}

struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            LandmarkList()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
